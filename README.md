Exercice
========

Ce projet a pour but de faire manipuler des branches. Initialement, il
contient deux branches, `main` et `dev`, qui pointent sur le même commit.

Suivre les instruction de l'[exo 4](https://gitlab.com/groupe-cours-git/exercices/-/blob/main/exo4.md) !
